//
//  ViewController.swift
//  Finders Keepers
//
//  Based on tutorial by Will Dages at https://willd.me/posts/getting-started-with-ibeacon-a-swift-tutorial
//
//

import UIKit
import CoreLocation

/* Global structure containing:
 - List of exercises not yet completed
 - Minor values of collected beacons (from QR codes)
 - The number of beacons at the start of a game
 */
struct GlobalVariables {
    static var globalExercises = ["Do star jumps for 2 minutes!","Do burpees for 2 minutes!","Do squats for 2 minutes!","Do high knees for 2 minutes!","Do sit ups for 2 minutes!","Do push ups for 2 minutes!", "Run on the spot for 2 minutes", "Jump on the spot for 2 minutes!", "Do the 'breaststroke' stretch for 2 minutes!", "Do the 'teapot' stretch for 2 minutes!"]
    static var collectedBeacons = Set<String>()
    static var totalBeacons = 6
}

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    let region = CLBeaconRegion(proximityUUID: UUID(uuidString: "444E98E8-59CC-1DDA-768F-83864D9BB361")!, identifier: "Estimotes")
    let beaconColors = [ // Set up background colours for displaying feedback on nearest beacon by associating them with Major ID values
        39203: UIColor(red: 84/255, green: 77/255, blue: 160/255, alpha: 1), // Purple
        44456: UIColor(red: 142/255, green: 212/255, blue: 220/255, alpha: 1), // Blue
        11785: UIColor(red: 162/255, green: 213/255, blue: 181/255, alpha: 1), // Green
        37030: UIColor(red: 84/255, green: 77/255, blue: 160/255, alpha: 1),
        11867: UIColor(red: 142/255, green: 212/255, blue: 220/255, alpha: 1),
        58142: UIColor(red: 162/255, green: 213/255, blue: 181/255, alpha: 1)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse) {
            locationManager.requestWhenInUseAuthorization()
        }
        // Start scanning for beacons
        locationManager.startRangingBeacons(in: region)
        
        // If all jewels collected, end game.
        let remainingJewelsCount = GlobalVariables.totalBeacons - GlobalVariables.collectedBeacons.count
        if remainingJewelsCount <= 0 {
            closestJewelDisplay.text = "You've collected all the jewels, well done!!!"
            remainingJewelsDisplay.isHidden = true
            foundJewelButton.isHidden = true
            
        }
        remainingJewelsDisplay.text = "\(remainingJewelsCount) Jewels Left"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        let knownBeacons = beacons.filter{ $0.proximity != CLProximity.unknown }
        
        // Code for displaying relevant feedback to player
        if (GlobalVariables.totalBeacons - GlobalVariables.collectedBeacons.count) > 0 {
            if (knownBeacons.count > 0) {
                let closestBeacon = knownBeacons[0] as CLBeacon
                if GlobalVariables.collectedBeacons.contains(closestBeacon.minor.stringValue) {
                    self.view.backgroundColor = self.beaconColors[closestBeacon.minor.intValue]
                    closestJewelDisplay.text = "You've already found this jewel - Look somewhere else!"
                }
                else {
                    self.view.backgroundColor = self.beaconColors[closestBeacon.minor.intValue]
                    closestJewelDisplay.text = "Jewel Nearby!"
                }
            }
            else {
                self.view.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
                closestJewelDisplay.text = "There are no jewels here - Keep looking!"
            }
        }
        
    }
    
    @IBOutlet var foundJewelButton: UIButton!
    @IBOutlet var closestJewelDisplay: UILabel!
    @IBOutlet var remainingJewelsDisplay: UILabel!
    
    // Reset globalvariables if new game is started 
    @IBAction func newGameButton(_ sender: UIButton) {
        GlobalVariables.collectedBeacons.removeAll()
        GlobalVariables.globalExercises = ["Do star jumps for 2 minutes!","Do burpees for 2 minutes!","Do squats for 2 minutes!","Do high knees for 2 minutes!","Do sit ups for 2 minutes!","Do push ups for 2 minutes!", "Run on the spot for 2 minutes", "Jump on the spot for 2 minutes!", "Do the 'breaststroke' stretch for 2 minutes!", "Do the 'teapot' stretch for 2 minutes!"]
        closestJewelDisplay.text = "There are no jewels here - Keep looking!"
        remainingJewelsDisplay.isHidden = false
        foundJewelButton.isHidden = false
        let remainingJewelsCount = GlobalVariables.totalBeacons - GlobalVariables.collectedBeacons.count
        remainingJewelsDisplay.text = "\(remainingJewelsCount) Jewels Left"

    }
    
}

