//
//  InstructionController.swift
//  Finders Keepers
//
//  Copyright © 2017 Andrew Bell. All rights reserved.
//

import UIKit

class InstructionController: UIViewController {

    @IBOutlet var instructionDisplayLabel: UILabel!
    @IBOutlet var countDownLabel: UILabel!
    @IBOutlet var challengeCompleteButton: UIButton!
    
    var counter = 126
    let exerciseLength = 120
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If list is empty refill it and repeat exercises (will never happen with only 6 beacons)
        if GlobalVariables.globalExercises.count == 0 {
            GlobalVariables.globalExercises = ["Do star jumps for 2 minutes!","Do burpees for 2 minutes!","Do squats for 2 minutes!","Do high knees for 2 minutes!","Do sit ups for 2 minutes!","Do push ups for 2 minutes!", "Run on the spot for 2 minutes", "Jump on the spot for 2 minutes!", "Do the 'breaststroke' stretch for 2 minutes!", "Do the 'teapot' stretch for 2 minutes!"]
        }
        let exercises = GlobalVariables.globalExercises

        // Select random exercise to display and remove it from list
        let randomIndex = Int(arc4random_uniform(UInt32(exercises.count)))
        instructionDisplayLabel.text = exercises[randomIndex]
        GlobalVariables.globalExercises.remove(at: randomIndex)
        
        
         var _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }

    // Decrement timer and display relevant message depending on what stage the timer is at
    func updateCounter() {
        
        if counter == (exerciseLength + 2) {
            countDownLabel.text = "Start in \(counter - exerciseLength - 1) second!"
            counter -= 1
        }
        else if counter == (exerciseLength + 1) {
            countDownLabel.text = "Start!"
            counter -= 1
        }
        else if counter > exerciseLength {
            countDownLabel.text = "Start in \(counter - exerciseLength - 1) seconds!"
            counter -= 1
        }
        else if counter > 1 {
            countDownLabel.text = "\(counter) seconds left!"
            counter -= 1
        }
        else if counter == 1 {
            countDownLabel.text = "\(counter) second left!"
            counter -= 1
        }
        else {
            countDownLabel.text = "Challenge complete!"
            challengeCompleteButton.isHidden = false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
